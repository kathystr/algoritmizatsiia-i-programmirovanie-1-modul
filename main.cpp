#include <iostream>
#include "matrix.h"
#include <locale>
using namespace std;
int main() {
	setlocale(LC_ALL, "rus");
	matrix a;
	a.input();
	cout << "Матрица 1: "<<endl;
	cout << a;
	matrix b;
	b.input();
	cout << "Матрица 2: " << endl;
	cout << b;
	try {
		int r, co;
		matrix c;
		cout << "Введите номер столбца и номер строки матрицы 1:";
		cin >> co >> r;
		cout << "a[" << r <<"]["<< co<<"]="<< a(r,co)<<endl;
		if (a.getcols() != b.getrows())
			throw 20;
		c = a * b;
		cout << "Результат умножения матриц: " << endl;
		cout << c;
		if (a.getcols() != b.getcols() || a.getrows() != b.getrows())
			throw 10;
		c = a + b;
		cout << "Результат сложения матриц 1 и 2:" << endl;
		cout << c;
		c = a - b;
		cout << "Результат вычитания матриц 1 и 2:" << endl;
		cout<<c;
	}
	catch (int i) {
		if (i == 10)
			cout << "Для операций сложения и вычитания матрицы должны быть одной размерности"<<endl;
		if(i==20)
			cout << "Нельзя умножать данные матрицы" << endl;

	}
	return 0;
}
