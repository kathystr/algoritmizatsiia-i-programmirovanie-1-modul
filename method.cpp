#include "matrix.h"
void matrix::input() {
	cout << "Введите количество столбцов, строк:" << endl;
	cin >> cols >> rows;
	a.resize(cols);
	for (unsigned i = 0; i < cols; ++i) {
		a[i].resize(rows);
	}
	for (unsigned i = 0; i < cols; ++i) {
		cout << "Введите " << rows << " элементов " << i + 1 << " строки:" << endl;
		for (unsigned j = 0; j < rows; j++) {
			cin >> a[i][j];
		}
	}
}
ostream& operator<<(ostream& os, const matrix& n){
	for (unsigned i = 0; i < n.cols; ++i) {
		for (unsigned j = 0; j < n.rows; j++) {
			os << n.a[i][j] << " ";
		}
		os<< endl;
	}
	os << endl;
	return os;
}
matrix matrix::operator+(const matrix &n)
{
	matrix result(*this);
	for (unsigned i = 0; i < cols; i++)
	{
		for (unsigned j = 0; j < rows; j++)
		{
			result.a[i][j] += n.a[i][j];
		}
	}
	return result;
}
matrix matrix::operator-(const matrix &n)
{
	matrix result(*this);

	for (unsigned i = 0; i < cols; i++)
	{
		for (unsigned j = 0; j < rows; j++)
		{
			result.a[i][j] -= n.a[i][j];
		}
	}
	return result;
}
matrix matrix::operator*(const matrix &n)
{
	matrix result(*this);
	for (unsigned i = 0; i < cols; i++)
		for (unsigned j = 0; j < n.rows; j++)
			for (unsigned k = 0; k < n.rows; k++)
				result.a[i][j] += a[i][j] * n.a[i][j];
	return result;
}
double& matrix:: operator()(unsigned r, unsigned c)
{
	if (r<0 || r>rows || c<0 || c>cols)
	{
		cout << "Введите корректное значение индексов";
		exit(10);
	}
	return a[r][c];
}
