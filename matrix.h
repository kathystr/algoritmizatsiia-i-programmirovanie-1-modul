#include <vector>
#include <iostream>
using namespace std;
class matrix {
private:
	unsigned cols, rows;//столбцы и строки
	friend ostream& operator<<(ostream&, const matrix&);
public:
	vector<vector<double>> a;
	matrix() {};
	matrix(int c,int r):cols(c),rows(r){}
	void input();
	int getcols() const {
		return cols;
	}
	int getrows()const {
		return rows;
	}
	matrix operator-(const matrix&);
	matrix operator+(const matrix&);
	matrix operator*(const matrix&);
	double& operator()(unsigned,unsigned);
	~matrix() {
		a.clear();
	}
};